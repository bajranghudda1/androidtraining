package com.intimetec.weeklyassignments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_rating.*

class RatingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        radioGroupRatingOption.setOnCheckedChangeListener { _, checkedId ->
            handleSelectedOption(checkedId)
        }

        ratingBarSong.setOnRatingBarChangeListener { _, rating, _ ->
            if (rating != 0f)
                Toast.makeText(requireContext(), "Rating is - $rating", Toast.LENGTH_SHORT).show()
        }
    }

    private fun handleSelectedOption(checkedId: Int) {
        if (checkedId == R.id.rbYes) {
            tvLikeTheSong.visibility = View.VISIBLE
            ratingBarSong.visibility = View.VISIBLE
            ratingBarSong.rating = 0f
        } else if (checkedId == R.id.rbNo) {
            tvLikeTheSong.visibility = View.GONE
            ratingBarSong.visibility = View.GONE
        }
    }

    companion object {
        const val fragmentName = "RatingFragment"
    }
}
