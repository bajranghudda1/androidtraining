package com.intimetec.weeklyassignments

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {

    private var fragment: Fragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //if state is already saved then get it from manager else create new
        if (savedInstanceState == null) {
            createFragment()
        } else {
            fragment = supportFragmentManager.getFragment(
                savedInstanceState,
                RatingFragment.fragmentName
            )!!
        }
    }

    private fun createFragment() {
        fragment = RatingFragment()
        supportFragmentManager.beginTransaction()
            .add(R.id.mainContainer, fragment!!).commit()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //Saving the fragment's instance
        supportFragmentManager.putFragment(outState, RatingFragment.fragmentName, fragment!!)
    }
}
